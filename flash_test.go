package main

import "testing"


var flash = NewClient("http://a:b@localhost:14022/", 6);


// 0.000001 FLC (flash) min

func TestSetFee(t *testing.T) {

  resp, err := flash.SetFee(0.000001)
  if err != nil {
     t.Errorf("setFee error: %+v", err)
     t.FailNow()
  }
  t.Logf("setFee result: %v", resp)
}

func TestCreateAddress(t *testing.T) {

	resp, err := flash.CreateAddress()
	if err != nil {
		 t.Errorf("createAddress error: %+v", err)
		 t.FailNow()
	}
	t.Logf("createAddress result: %v", resp)
}

func TestGetBalance(t *testing.T) {

  resp, err := flash.GetBalance()
  if err != nil {
     t.Errorf("getBalance error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalance result: %v", resp)
}

func TestGetWalletInfo(t *testing.T) {
  resp, err := flash.GetWalletInfo()
  if err != nil {
     t.Errorf("getWalletInfo error: %+v", err)
     t.FailNow()
  }
  t.Logf("getWalletInfo result: %+v", resp)
}

func TestGetBalanceByAddress(t *testing.T) {
  resp, err := flash.GetBalanceByAddress("UTqn5cYQDsymGBeWALzBAtPctG9USqQMxU")
  if err != nil {
     t.Errorf("getBalanceByAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalanceByAddress result: %v", resp)
}


func TestSendToAddress(t *testing.T) {

  addr := "UPWNj14PYM9aBUFCmsn4MqQcKu9Sm1aAcg"
  resp, err := flash.SendToAddress(addr, 0.01)
  if err != nil {
     t.Errorf("sendToAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("sendToAddress result: %v", resp)
}



func TestGetTransaction(t *testing.T) {
  resp, err := flash.GetTransaction("831915c54113c25bd64ccc1fe2d2c8d38391f171b4844f08ee6b2de5977a4614")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}

func TestCheckTransaction(t *testing.T) {
  resp, err := flash.CheckTransaction("831915c54113c25bd64ccc1fe2d2c8d38391f171b4844f08ee6b2de5977a4614")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}
